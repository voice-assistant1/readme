![banner](/content/elizabeth_banner.png)

Elizabeth is a voice assistant made in the first place as project to test my skills. Hopefully in the future it will be something more \^.\^

The main project challenges I try to achieve are:
* Fully customizable
* Easy to use
* Fast response


### Content:
1. Stack
    - Backend
    - Frontend
2. Instalation
3. Requirements
4. Used technologies
5. How to make your own package for Elizabeth?


### 1. Stack
**Backend stack:**
- Flask
- MongoDB

**Frontend stack:** 
* TKinter


### 2. Instalation
>```
> 1. git clone git@gitlab.com:voice-assistant1/services/application.git && cd application
> 2. python index.py
>```
At the moment Elizabeth dont have GUI, so you only can compile backend! Have fun \^.\^


### 3. Requirements
- Flask (Used for making API)
- openai (ChatGPT API)
- pymongo (For working with MongoDB)
- dotenv
- customtkinter (For GUI)
- PIL.Image


### 4. Used technologies
* **TTS**(text to speech) - currently not implemented
* **STT**(speech to text) - currently not implemented
* **User voice recognition**(to let Liz know with whom she is talking) - currently not implemented


### 5. How to make your own package for Elizabeth?
Make an python file with following name `package_*.py` and inside you must have `get_keywords()` and `initialize()` functions.

* `initialize()` is a main function for Elizabeth, so dont forget to call every other functions inside

* `get_keywords()` must return list of strings that contain all keywords.

**OOP Style**
```Python
class example:   
    def __init__(self) -> None:
        self.keywords = [
            "keyword1",
            "keyword2"
        ]

    def doSmth(self):
        return self.keywords

def get_keywords():
    return example().keywords

def initialize():
    return example().doSmth()
```

**Functional Style**
```Python
def doSmth():
    pass

def get_keywords():
    return ["keyword1","keyword2"]

def initialize():
    return doSmth()
```